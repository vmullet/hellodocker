#!/bin/bash

# To build the image from docker
docker image build -t xenogera/mywebserver .

# To push on dockerhub
docker push xenogera/mywebserver

# To remove the image from cache
docker image rm xenogera/mywebserver

# To create a container based on the image from dockerhub
docker container run --publish 80:80 --detach --name mywebserver xenogera/mywebserver