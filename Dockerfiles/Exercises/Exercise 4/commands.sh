#!/bin/bash

docker container run -p 8080:8080 -p 50000:50000 -d --name jenkins1 -v jenkins_volume:/var/jenkins_home jenkins:2.32.1

docker container rm -f jenkins1

docker container run -p 8080:8080 -p 50000:50000 -d --name jenkins2 -v jenkins_volume:/var/jenkins_home jenkins:latest